# Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

Modified by Yeaseul Shin for CIS 322 project 6
yshin@uoregon.edu

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help.

The modified version of the calculator attempted to eliminate such ambiguity.

## Time Zone
All time signatures are expressed in UTC.

## Metrics
Uses km for calculation. mile will be converted to km, vice versa.

## Max/Min Speed
Following calculator uses following chart for its calculation
possible total distance:
200, 300, 400, 600, 1000
possible max speed according to total distance:
34, 32, 30, 28, 26
possible min speed according to the total distance:
15, 15, 15, 11.428, 13.333

The max/min speed is determined by ONLY the total distance.
If total distance is 200 and control distance is 250, 600, 1000, then only 34km/hr will be used.
In fact, entering control distance that exceeds the next control distance will give inaccurate time.
For example, total distance of 200km will give desirable result as long as the control distance < 300.
Similarly, total distance of 300km will give desirable result as long as the control distance < 400

To deal with this is that change the total distance as the control distance exceeds each boundary;
Use 200km total distance if control distance < 300km
Use 300km total distance if 300km <= control distance < 400km
Use 400km total distance if 400km <= control distance < 600km

## Calculation
During the calculation of time, all numbers will be rounded to two decimal places. When shifting the given beginning time, the number will entered as an integer.

For example, during the calculation of how much time should be added, all decimals are rounded to two decimal places. Then the final number (with two decimal places) will rounded to a whole number.


## Database Interaction
This program enters opening and closing time in the field when submit button is clicked.

Clicking display button will display the items in the database in the separate html page.

test case#1
If no items in the database, then it will render an webpage that indicates that the database is empty.

## Api
This program suppose to have RESTful API. This functionalities  should be:
/listAll to list all of the open/close time
/listOpenOnly to list all of the open time
/listCloseOnly to list all of the close time

There is also a separate web page where the users can use such API
